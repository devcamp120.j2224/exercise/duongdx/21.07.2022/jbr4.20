package com.devcamp.s10.jbr4_20;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Jbr420Application {

	public static void main(String[] args) {
		SpringApplication.run(Jbr420Application.class, args);
	}

}
