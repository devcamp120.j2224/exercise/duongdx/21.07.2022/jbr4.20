package com.devcamp.s10.jbr4_20;

public class Book {
    private String name ;
    private Author author ;
    private int qty = 0;
    private double price ;
    
    public Book(String name, Author author, int qty, double price) {
        this.name = name;
        this.author = author;
        this.qty = qty;
        this.price = price;
    }

    public Book(String name, Author author, double price) {
        this.name = name;
        this.author = author;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Book [Author [ name=" + author.getName() + ", email=" + author.getEmail() + ", gender=" + author.getGender() + "]" + ", name=" + name + ", price=" + price + ", qty=" + qty + "]";
    }

    
}
