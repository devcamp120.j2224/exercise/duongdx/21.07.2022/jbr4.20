package com.devcamp.s10.jbr4_20;

import java.util.ArrayList;
import org.springframework.web.bind.annotation.*;

@RestController
public class BookController {
    @CrossOrigin
    @GetMapping("/books")
    public ArrayList<Book> getListbook(){
        Author TacGia1 = new Author("To Huu", "tohuu@gmail.com", 'm');
        Author TacGia2 = new Author("Vu Trong Phung", "VuTrongphun@gmail.com", 'm');
        Author TacGia3 = new Author("ho Xuan Huong", "hoXuanHuong@gmail.com", 'f');

        System.out.println(TacGia1);
        System.out.println(TacGia2);
        System.out.println(TacGia3);

        Book Sach1 = new Book("DeMen",TacGia1, 60000);
        Book Sach2 = new Book("Hanh Phuc Cua mot tang gia", TacGia2, 3, 45000);
        Book Sach3 = new Book("banh troi nuoc", TacGia3, 2, 35000);

        System.out.println("====Sach===");
        System.out.println(Sach1);
        System.out.println(Sach2);
        System.out.println(Sach3);

        ArrayList<Book> danhSachTacPham = new ArrayList<>();
        danhSachTacPham.add(Sach1);
        danhSachTacPham.add(Sach2);
        danhSachTacPham.add(Sach3);
        return danhSachTacPham ;
    }
}
